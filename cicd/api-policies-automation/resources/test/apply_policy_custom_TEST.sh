#!/bin/bash
# Usage: ./api_manager_cli_test.sh <username> <password>
apimClient=../../api_manager_cli.sh
orgName="Bank Of Ireland"
username=$1
password=$2
apiName="Notes Example API"
apiVersion="2.0.2"
policyName="Demo Test"
envName="Sandbox"
region="eu1"
echo "Applying Custom Policy - Test"
#Note: --policy-data is policy configuration data required. The body of thi json message must be set for the particular policy we are applying

$apimClient apply-policy --policy-type custom --region eu1 -u $username -p "$password" -o "$orgName" -e "$envName" \
            --policy-name "$policyName" \
            --api-name "$apiName"  --api-version "$apiVersion" \
            --policy-data '{ "dn-header-name" : "DN" }'
