#!/bin/bash
# Usage: ./api_manager_cli_test.sh <username> <password>
apimClient=../../api_manager_cli.sh
orgName="Bank Of Ireland"
username=$1
password=$2
apiName="Notes Example API"
apiVersion="2.0.2"
policyName="Rate Limiting"
envName="Sandbox"

echo "Applying Custom Policy - Test "
$apimClient apply-policy --region eu1 -u "$username" -p "$password" -o "$orgName" -e "$envName" \
            --policy-name "$policyName" \
             --api-name "$apiName"  --api-version "$apiVersion"
