#!/bin/bash
# Usage: ./api_manager_cli_test.sh <username> <password>
apimClient=../../api_manager_cli.sh
orgName="Bank Of Ireland"
username=$1
password=$2
policyName="Demo Test"

echo "Creating Policy Template - Test "
$apimClient create-policy --region eu1 -o "$orgName" -u "$username" -p "$password" \
  --policy-name "$policyName" \
  --policy-definition "cicd-sample-policy/dn-cid-policy.yaml" \
  --policy-configuration "cicd-sample-policy/dn-cid-policy.xml"
