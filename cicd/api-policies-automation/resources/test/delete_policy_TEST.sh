#!/bin/bash
# Usage: ./api_manager_cli_test.sh <username> <password>
apimClient=../../api_manager_cli.sh
orgName="Bank Of Ireland"
username=$1
password=$2
policyName="TEST - DN - Client Id Validation (CICD Test Policy do not use) "

echo "Removing Policy Template - Test "
$apimClient delete-policy --region eu1 -o "$orgName" -u "$username" -p "$password" --policy-name "$policyName"
