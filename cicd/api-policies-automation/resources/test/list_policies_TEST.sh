#!/bin/bash
# Usage: ./api_manager_cli_test.sh <username> <password>
apimClient=../../api_manager_cli.sh
orgName="Bank Of Ireland"
username=$1
password=$2

echo "Listing Policy Templates - Test"
$apimClient list-policies --region eu1 -o "$orgName" -u "$username" -p "$password"
