#!/bin/bash

# Functions

# Parse input parameters and set variables required
init(){
    args="$@ --"
    echo "Args: $args"

    # Default values
    policyType="ootb"
    # Platform API endpoints - Default Region: US
    accAPI='https://anypoint.mulesoft.com/accounts'
    apimAPI='https://anypoint.mulesoft.com/apimanager/api/v1'
    apiPlatform='https://anypoint.mulesoft.com/apiplatform/repository/v2'

    #Parse action value.
    action=$1
    shift

    while [ $# -ge 1 ]; do
            case "$1" in
                    --)
                        # No more options left.
                        shift
                        break
                       ;;
                    -u|--username)
                            username="$2"
                            echo "username=$username"
                            shift
                            ;;
                    -p|--password)
                            password="$2"
                            echo "password=****"
                            shift
                            ;;
                    -o|--organization)
                            orgName="$2"
                            echo "organization=$orgName"
                            shift
                            ;;
                    -e|--environment)
                            envName="$2"
                            echo "envName=$envName"
                            shift
                            ;;
                    --region)
                            region="$2"
                            echo "region=$region"
                            shift
                            ;;
                    --policy-name)
                            policyName="$2"
                            echo "policyName=$policyName"
                            shift
                            ;;
                    --policy-definition)
                            definitionFile="$2"
                            echo "definitionFile=$definitionFile"
                            shift
                            ;;
                    --policy-configuration)
                            configurationFile="$2"
                            echo "configurationFile=$configurationFile"
                            shift
                            ;;
                    --policy-data)
                            policyData="$2"
                            echo "policyData=$policyData"
                            shift
                            ;;
                    --api-name)
                            apiName="$2"
                            echo "apiName=$apiName"
                            shift
                            ;;
                    --api-version)
                            apiVersion="$2"
                            echo "apiVersion=$apiVersion"
                            shift
                            ;;
                    --policy-type)
                            policyType="$2"
                            echo "policyType=$policyType"
                            shift
                            ;;
            esac
            shift
    done
    if [ "$region" == "eu1" ]; then
        # Platform API endpoints - Europe
        apimAPI='https://eu1.anypoint.mulesoft.com/apimanager/api/v1'
        accAPI='https://eu1.anypoint.mulesoft.com/accounts'
        apiPlatform='https://eu1.anypoint.mulesoft.com/apiplatform/repository/v2'
    fi

}

# username
# password
get_access_token() {
    # Authenticate with user credentials (Note the APIs will NOT authorize for tokens received from the OAuth call. A user credentials is essential)
    echo "Getting access token from $accAPI/login..."
    accessToken=$(curl -s $accAPI/login -X POST -d "username=$1&password=$2" | jq --raw-output .access_token)
    echo "Access Token: $accessToken"
}

# access_token
# org_name
get_organization_id() {
    # Pull org id from my profile info
    echo "Getting org ID from $accAPI/api/me..."
    echo "Org Name = $2"
    jqParam=".user.contributorOfOrganizations[] | select(.name==\"$2\").id"
    orgId=$(curl -s $accAPI/api/me -H "Authorization:Bearer $1" | jq --raw-output "$jqParam")
    echo "Organization ID: $orgId"

}

# access_token
# org_id
# env_name
get_environment_id() {
    # Pull env id from matching env name
    echo "Getting env ID from $accAPI/api/organizations/$2/environments..."
    echo "Environment Name: $3"
    #echo "get_environment_id response: $(curl -s $accAPI/api/organizations/$orgId/environments -H "Authorization:Bearer $1")"
    jqParam=".data[] | select(.name==\"$3\").id"
    envId=$(curl -s $accAPI/api/organizations/$orgId/environments -H "Authorization:Bearer $1" | jq --raw-output "$jqParam")
    echo "Environment ID: $envId"
}

# access_token
# org_id
get_custom_policy_templates() {
  echo "Retrieving list of Custom Policy Templates"
  accessToken=$1
  orgId=$2
  curl --resolve GET --url "$apimAPI/organizations/$orgId/custom-policy-templates"\
    --header "Authorization: Bearer $accessToken" \
    --header "Content-Type: application/json"
}


# access_token
# org_id
# policy_name
# definition_file
# configuration_file
create_custom_policy_template() {
    accessToken=$1
    orgId=$2
    name="$3"
    echo "Creating Custom Policy Template: $policyName"
    # ex. /Users/username/Documents/dn-cid-validation/dn-cid-policy.yaml
    definition="$4"
    echo "Definition File: $definition"
    # ex. /Users/username/Documents/dn-cid-validation/dn-cid-policy.xml
    configuration="$5"
    echo "Configuration File: $configuration"

    curl --resolve POST --url "$apimAPI/organizations/$orgId/custom-policy-templates" \
      --header "Authorization: Bearer $accessToken" \
      --header "Content-Type: multipart/form-data" \
      --form "name=$name" \
      --form "gatewayVersion=*" \
      --form "definition=@$definition" \
      --form "configuration=@$configuration"
}

# access_token
# org_id
# policy_name
delete_custom_policy_template() {
    accessToken=$1
    orgId=$2
    policyName="$3"

    echo "Resolving Custom Policy Id for policy: $policyName"
    policies=$(curl --resolve GET --url "$apimAPI/organizations/$orgId/custom-policy-templates" --header "Authorization: Bearer $accessToken" --header "Content-Type: application/json")
    jqParam=".customPolicyTemplates[] | select(.name==\"$policyName\").id"
    customPolicyTemplateId=$(echo $policies | jq --raw-output "$jqParam")

    echo "Deleting Custom Policy Template: $policyName"
    curl --request DELETE --url "$apimAPI/organizations/$orgId/custom-policy-templates/$customPolicyTemplateId" \
      --header "Authorization: Bearer $accessToken" \
      --header "Content-Type: application/json"
}

# access_token
# org_id
# policy_name
# environment_name
# api_name
# api_version
# policy_configuration_data
apply_policy() {

    accessToken=$1
    orgId=$2
    policyName="$3"
    envName="$4"
    apiName="$5"
    apiVersion="$6"
    policyConfigurationData="$7"

    # Resolve Env Id
    echo "Retrieving Environment Id for $envName"
    environments=$(curl --resolve GET --url "$apiPlatform/organizations/$orgId/environments" --header "Authorization: Bearer $accessToken")
    #echo "Environments List: $environments"
    jqParam=" .[] | select(.name==\"$envName\").id "
    envId=$(echo $environments | jq --raw-output "$jqParam")

    if [ -z "$envId" ];  then
      echo "Could not resolve environment Id for $envName"
      exit 1
    else
      echo "Environment Id: $envId"
    fi

    # Check Policy Type
    if [ "$policyType" == "custom" ]; then
      echo "Resolving Custom Policy Id for policy: $policyName"
      templatesResource="custom-policy-templates"
    else
      echo "Resolving OOTB Policy Id for policy: $policyName"
      templatesResource="policy-templates"
    fi

    # Resolve Policy Template Id
    policies=$(curl --resolve GET --url "$apimAPI/organizations/$orgId/$templatesResource" --header "Authorization: Bearer $accessToken")
    # echo "Policy Templates List: $policies"
    jqParam=" .customPolicyTemplates[] | select(.name==\"$policyName\").id "
    policyTemplateId=$(echo $policies | jq --raw-output "$jqParam")

    if [ -z "$policyTemplateId" ];  then
      echo "No Custom Policy Template Found with Name: $policyName"
      exit 1
    else
      echo "Policy Template Id: $envId"
    fi

    # TODO Resolve API env Id
    echo "Resolving API Environment Id for: $apiName, version: $apiVersion"
    assets=$(curl --resolve GET --url "$apimAPI/organizations/$orgId/environments/$envId/apis" --header "Authorization: Bearer $accessToken")
    echo "APIs List: $assets"
    jqParam=" .assets[] | select(.exchangeAssetName==\"$apiName\").totalApis "
    totalApis=$(echo $assets | jq --raw-output "$jqParam")
    if [ -z "$totalApis" ];  then
      echo "No API Instances found for: $apiName, version: $apiVersion"
      exit 1
    else
      echo "Total API Instances found=$totalApis"
    fi

    echo "Resolving API Environment Id for: $apiName, version: $apiVersion"

    #Select instance for this env
    jqParam=".assets[] | select(.exchangeAssetName==\"$apiName\").apis"
    apiInstances=$(echo $assets | jq --raw-output "$jqParam")
    if [ -z "$apiInstances" ];  then
      echo "No API Instances found for API: $apiName, version: $apiVersion "
      exit 1
    else
      echo "apiInstances=$apiInstances"
    fi

    #Select API instance(s) for this env
    jqParam=" .[] | select(.productVersion==\"$apiVersion\" and .environmentId==\"$envId\").id "
    apiEnvIds=$(echo $apiInstances | jq --raw-output "$jqParam" )

    if [ -z "$apiEnvIds" ];  then
      echo "No matching API Instance can be found for API: $apiName, version: $apiVersion, env: $envName "
      exit 1
    else
      #Apply policy
        # Save current IFS
        SAVEIFS=$IFS
        # Change IFS to new line.
        IFS=$'\n'
        apiEnvIds=($apiEnvIds)
        # Restore IFS
        IFS=$SAVEIFS
        echo ""
        echo "Using Policy Configuration = $policyConfigurationData"
        echo ""

      for apiInstanceId in "${apiEnvIds[@]}"
        do
         echo ">> Applying policy: \"$policyName\", target API Instance (apiEnvId): "$apiInstanceId", API name: \"$apiName\",  version:\"$apiVersion\",  Env:\"$envName\" "
         curl --resolve POST --url "$apimAPI/organizations/$orgId/environments/$envId/apis/$apiInstanceId/policies" \
           --header "Authorization: Bearer $accessToken" \
           --header 'Content-Type: application/json' \
           --data "{ \"policyTemplateId\": ${policyTemplateId},  \"configurationData\": ${policyConfigurationData} }"
         echo ""
        done
     fi
}

help_menu(){
  echo "########################################"
  echo "### API Manager CLI - Help menu ###"
  echo " $> api_manager_cli.sh ( list-policies | create-policy | delete-policy | apply-policy  ) [ --region eu1 ] -u|--username {username} -p|--password {password} -o|--organization {orgName} -e|--environment {envName} "
  echo ""
  echo "### Example - list"
  echo " $> api_manager_cli.sh list-policies --region eu1 -u username -p password -o \"My Anypoint Org\" "
  echo ""
  echo "### Example - create"
  echo " $> api_manager_cli.sh create-policy --region eu1 -u username -p password -o \"My Anypoint Org\" --policy-name \"cicd-sample-policy\" \
    --policy-definition \"cicd-sample-policy/dn-cid-policy.yaml\" \
    --policy-configuration \"cicd-sample-policy/dn-cid-policy.xml\""
  echo ""
  echo "### Example - delete"
  echo " $> api_manager_cli.sh delete-policy --region eu1 -u username -p password -o \"My Anypoint Org\"  --policy-name \"cicd-sample-policy\" "
  echo ""
  echo "### Example - apply"
  echo " $> api_manager_cli.sh  apply-policy --policy-type custom --region eu1 -u username -p pass -o \"My Anypoint Org\" -e \"Sandbox\" \
              --policy-name "" \
              --api-name \"API Name\"  --api-version \"1.0.0\" \
              --policy-data '{ \"attribute\" : \"value\" }'"
  echo "########################################"
  exit 0
}


# Script Body
echo " >> Parsing input parameters"
init "$@"
echo " >> Processing action: $action"
case $action in
  list-policies)
        get_access_token "$username" "$password"
        get_organization_id $accessToken "$orgName"
        get_custom_policy_templates $accessToken $orgId
        ;;
  create-policy)
        get_access_token "$username" "$password"
        get_organization_id $accessToken "$orgName"
        create_custom_policy_template $accessToken $orgId "$policyName" "$definitionFile" "$configurationFile"
        ;;
  delete-policy)
        get_access_token "$username" "$password"
        get_organization_id $accessToken "$orgName"
        delete_custom_policy_template $accessToken $orgId "$policyName"
        ;;
  apply-policy)
        get_access_token "$username" "$password"
        get_organization_id $accessToken "$orgName"
        apply_policy $accessToken $orgId "$policyName" "$envName" "$apiName" "$apiVersion" "$policyData"
        ;;
  help|-h|--help)
        help_menu
        ;;
  *)
        echo  " >> Invalid input. Please specify a valid action, one of: list-policies | create-policy | delete-policy | apply-policy | help "
        exit 0
        ;;
esac
echo ""
echo "Done."
