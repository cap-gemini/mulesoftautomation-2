#!/bin/bash

# Functions

# Parse input parameters and set variables required
init(){
    args="$@"
    echo "Args: $args"

    accAPI='https://eu1.anypoint.mulesoft.com/accounts'
    apiPlatform='https://eu1.anypoint.mulesoft.com/apiplatform/repository/v2'
	armAPI='https://eu1.anypoint.mulesoft.com/hybrid/api/v1'

    #Parse action value.
    action=$1
    shift

    while [ $# -ge 1 ]; do
            case "$1" in
                    --)
                        # No more options left.
                        shift
                        break
                       ;;
                    -u|--username)
						username="$2"
						echo "username=$username"
						shift
						;;
                    -p|--password)
						password="$2"
						echo "password=*****"
						shift
						;;
                    -o|--organization)
						orgName="$2"
						echo "organization=$orgName"
						shift
						;;
                    -e|--environment)
						envName="$2"
						echo "envName=$envName"
						shift
						;;
                    --api-name)
						apiName="$2"
						echo "apiName=$apiName"
						shift
						;;
					--file-name)
						fileName="$2"
						echo "fileName=$fileName"
						shift
						;;
					--target)
						target="$2"
						echo "target=$target"
						shift
						;;
            esac
            shift
    done
}

# username
# password
get_access_token() {
    echo "Getting Access Token..."
    accessToken=$(curl -s $accAPI/login -X POST -d "username=$1&password=$2" | jq --raw-output .access_token)
	
	if [ -z "$accessToken" ];  then
		echo "Could not retrieve Access Token"
		exit 1
    else
		echo "Access Token: $accessToken"
		echo ""
    fi
}

# access_token
# org_name
get_organization_id() {
    echo "Getting Organization ID..."
    echo "Organization Name = $2"
    jqParam=".user.contributorOfOrganizations[] | select(.name==\"$2\").id"
    orgId=$(curl -s $accAPI/api/me -H "Authorization:Bearer $1" | jq --raw-output "$jqParam")
    
	if [ -z "$orgId" ];  then
		echo "Could not resolve Organization ID for $2"
		exit 1
    else
		echo "Organization ID: $orgId"
		echo ""
    fi
}

# access_token
# org_id
# env_name
get_environment_id() {
    echo "Getting Environment ID..."
    echo "Environment Name: $3"
    jqParam=".[] | select(.name==\"$3\").id"
    envId=$(curl -s $apiPlatform/organizations/$2/environments -H "Authorization:Bearer $1" | jq --raw-output "$jqParam")
    
	if [ -z "$envId" ];  then
		echo "Could not resolve Environment ID for $3"
		exit 1
    else
		echo "Environment ID: $envId"
		echo ""
    fi
}

# access_token
# org_name
# env_name
# org_id
# env_id
# target
get_server() {
	echo "Getting Server Group for Organization: $2, Environment: $3..."
	jqParam=".data[] | select(.name==\"$6\").id"
	serverGroupId=$(curl -s $armAPI/serverGroups -H "Authorization:Bearer $1" -H "x-anypnt-org-id:$4" -H "x-anypnt-env-id:$5" | jq --raw-output "$jqParam")
	
	if [ -n "$serverGroupId" ];  then
		echo "Found Server Group - $6..."
		echo ""
		targetId=($serverGroupId)
	else
		echo "No Server Group found..."
		echo ""
		echo "Getting Server for Organization: $2, Environment: $3..."
		jqParam=".data[] | select(.name==\"$6\").id"
		serverId=$(curl -s $armAPI/servers -H "Authorization:Bearer $1" -H "x-anypnt-org-id:$4" -H "x-anypnt-env-id:$5" | jq --raw-output "$jqParam")
		
		if [ -z "$serverId" ];  then
			echo "No Server found..."
			exit 1
		else
			echo "Found Server - $6..."
			echo ""
			targetId=($serverId)
		fi
	fi
}

# access_token
# org_id
# env_id
# api_name
# target_id
# target_name
# file_name
deploy_application() {
	echo "Getting deployed Applications..."
	jqParam=".data[] | select(.name==\"$4\" and .target.name==\"$6\").id"
	appId=$(curl -s $armAPI/applications -H "Authorization:Bearer $1" -H "x-anypnt-org-id:$2" -H "x-anypnt-env-id:$3" | jq --raw-output "$jqParam")
	
	if [ -z "$appId" ];  then
		echo "Deploying Application $4 on $6..."
		response=$(curl -s $armAPI/applications -X POST -H "Authorization:Bearer $1" -H "x-anypnt-org-id:$2" -H "x-anypnt-env-id:$3" -F "artifactName=$4" -F "targetId=$5" -F "file=@$7")
		echo "Response: $response"
	else
		echo "Application $4 is already deployed on $6..."
		echo "Updating Application..."
		response=$(curl -s $armAPI/applications/$appId -X PATCH -H "Authorization:Bearer $1" -H "x-anypnt-org-id:$2" -H "x-anypnt-env-id:$3" -F "file=@$7")
		echo "Response: $response"
	fi
}

help_menu(){
	echo "########################################"
	echo "### Runtime Manager CLI - Help menu ###"
	echo "### Example - Deploy Application "
	echo " $> runtime_manager_cli.sh deploy-application -u username -p password -o \"CMA - Live\" -e \"AWS-CMA-DEV\" --api-name \"ob-internal-v1\" --file-name \"ob-internal-api-proxy-1.0.0-SNAPSHOT.zip\" --target \"muleserver-cmatest\" "
	echo "########################################"
	exit 0
}


# Script Body
echo ""
echo " >> Parsing input parameters"
init "$@"
echo ""
echo " >> Processing action: $action"
echo ""
case $action in
  deploy-application)
        get_access_token "$username" "$password"
        get_organization_id "$accessToken" "$orgName"
		get_environment_id "$accessToken" "$orgId" "$envName"
		get_server "$accessToken" "$orgName" "$envName" "$orgId" "$envId" "$target"
		deploy_application "$accessToken" "$orgId" "$envId" "$apiName" "$targetId" "$target" "$fileName"
        ;;
  help|-h|--help)
        help_menu
        ;;
  *)
        echo  " >> Invalid input. Please specify a valid action. "
        exit 0
        ;;
esac
echo ""
echo "Done."
