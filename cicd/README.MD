# MuleSoft CI/CD Guidelines

These guidelines and assets will enable MuleSoft APIs Continuous Integration and Deployment for the EU Management Plane.

Before going through this guide, it is required to have Maven 3.x installed and configured.

Also it is important to be familiarized with the Mule Maven documentation (https://docs.mulesoft.com/mule-user-guide/v/3.8/using-maven-with-mule) before going through these guidelines.

## Modules

* `boi-cicd-demo-api` is a Mulesoft Application, created as an example of an API implementation.
   Its `pom.xml` can be used as a reference for automating deployment of the apis/applications created.


## API Deployment (Hybrid)

Automated deployment using the MuleSoft Maven Plugin: ```mule-maven-plugin```.
This allows us to deploy applications to hosted runtimes via Anypoint Runtime Manager using a server, serverGroup or cluster.
It will be required to provide the corresponding Anypoint Platform credentials, Anypoint Organization Id, and configure the target cluster/server name.

## Steps
1. Add the ```mule-maven-plugin``` to all APIs pom.xml files.

Example
```		
		<plugin>
				<groupId>org.mule.tools.maven</groupId>
				<artifactId>mule-maven-plugin</artifactId>
				<version>2.2.1</version>
				<configuration>
					<deploymentType>arm</deploymentType>
					<username>${anypoint.username}</username>
					<password>${anypoint.password}</password>
					<target>${arm.target.name}</target>
					<targetType>${arm.target.type}</targetType>
					<environment>${arm.target.environment}</environment>
					<uri>${anypoint.uri}</uri>
				</configuration>
				<executions>
					<execution>
						<id>mule-deploy</id>
						<phase>deploy</phase>
						<goals>
							<goal>deploy</goal>
						</goals>
					</execution>
				</executions>
		</plugin>
```

2. Add the properties to externalize configuration values for the ```mule-maven-plugin``` plugin.

Example
```
	<properties>
	(...)
		<!-- Mule Maven Plugin params -->
		<!-- Anypoint URI. US (default): https://anypoint.mulesoft.com. EU: https://eu1.anypoint.mulesoft.com -->
		<anypoint.uri>https://eu1.anypoint.mulesoft.com</anypoint.uri>
    <anypoint.username>${anypoint_user}</anypoint.username>
		<anypoint.password>${anypoint_pass}</anypoint.password>
		<!-- Target Name: cluster, serverGroup or server name -->
		<arm.target.name>jmporcel-test-cluster-15</arm.target.name>
		<!-- Target Type: one of [ cluster | serverGroup | server ] -->
		<arm.target.type>cluster</arm.target.type>
		<!-- Name of Runtime Manager Environment -->
		<arm.target.environment>Sandbox</arm.target.environment>
	</properties>
```

3. Exchange Repository Configuration.

3.1. Add Exchange Server to Maven Settings

In your Maven `settings.xml` file, you  need to configure your credentials for the Exchange Maven Facade.

```
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
  <servers>
    <server>
      <id>exchange2-boi-eu-repository</id>
      <username>username</username>
      <password>password</password>
    </server>
  </servers>
</settings>
```

3.2. Make sure you include the distribution management section in your application/parent `pom.xml`.

```
	<distributionManagement>
		<!-- Target Anypoint Organization Repository -->
		<repository>
     	    <id>exchange2-boi-eu-repository</id>
			<name>Exchange2 Repository</name>
			<url>https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/${anypoint.org.id}/maven</url>
			<layout>default</layout>
		</repository>
	</distributionManagement>
```

3.3. Add Classifier for Echange Asset

Publication of Mule 3.x projects is fully supported by Exchange Maven Facade. However, in order to publish some types of projects, an extra property is needed in the POM file. For Mule Applications we should use 'app' type.

```
  <properties>
    <type>app</type>
  </properties>
```

If you want to know more about publishing assets to Exchange using Maven, refer to:
* MuleSoft Docs: https://docs.mulesoft.com/anypoint-exchange/to-publish-assets-maven
* Exchange Maven Facade API: https://anypoint.mulesoft.com/apiplatform/anypoint-platform/#/portals/organizations/2559c34c-bbc7-4a5a-b078-bc1154594906/apis/5238933/versions/100008

4. Deployment and Delivery.

The deployment process must at a minimum guarantee the generated artifact is deployed to the target runtime server.
For this purpose, invoking the mule maven deploy plugin is sufficient.
If we also want to deliver the artifact to be reused in Exchange, or any other artifact repository (ex. Nexus, Artifactory), then `distributionManagement` attribute must be configured in `pom.xml` as shown in **3.2**.

4.1. Deployment to ARM (only).

Alternatively, you can run `mvn clean install` and, `mvn org.mule.tools.maven:mule-maven-plugin:2.2.1:deploy -Danypoint_user=<username> -Danypoint_pass=<password>` to just deploy the app to the target runtime, i.e. no deploy the asset to the Exchange repository.

Example Console Output
```
juan.porcel@BA-JPORCE-OSX:~/AnypointStudio/workspace644/boi-cicd-demo-api$ mvn org.mule.tools.maven:mule-maven-plugin:2.2.1:deploy -Danypoint_user=myuser -Danypoint_pass=mypass
[INFO] Scanning for projects...
[INFO]
[INFO] ---------< 65e99896-9aea-448e-b484-9a13e3f23349:boi-cicd-demo >---------
[INFO] Building boi-cicd-demo-api 1.0.0-SNAPSHOT
[INFO] --------------------------------[ mule ]--------------------------------
[INFO]
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ boi-cicd-demo ---
[INFO] Deleting /Users/juan.porcel/AnypointStudio/workspace644/boi-cicd-demo-api/target
[INFO]
[INFO] --- mule-app-maven-plugin:1.2:attach-test-resources (default-attach-test-resources) @ boi-cicd-demo ---
[INFO] attaching test resource /Users/juan.porcel/AnypointStudio/workspace644/boi-cicd-demo-api/src/main/app
[INFO]
[INFO] --- build-helper-maven-plugin:1.7:add-resource (add-resource) @ boi-cicd-demo ---
[INFO]
[INFO] --- maven-resources-plugin:3.0.2:resources (default-resources) @ boi-cicd-demo ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources
[INFO] Copying 3 resources
[INFO] Copying 1 resource
[INFO] skip non existing resourceDirectory /Users/juan.porcel/AnypointStudio/workspace644/boi-cicd-demo-api/mappings
[INFO]
[INFO] --- mule-app-maven-plugin:1.2:filter-resources (default-filter-resources) @ boi-cicd-demo ---
[INFO]
[INFO] --- maven-compiler-plugin:3.7.0:compile (default-compile) @ boi-cicd-demo ---
[INFO] Nothing to compile - all classes are up to date
[INFO]
[INFO] --- maven-resources-plugin:3.0.2:testResources (default-testResources) @ boi-cicd-demo ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 1 resource
[INFO] Copying 3 resources
[INFO]
[INFO] --- maven-compiler-plugin:3.7.0:testCompile (default-testCompile) @ boi-cicd-demo ---
[INFO] Nothing to compile - all classes are up to date
[INFO]
[INFO] --- maven-surefire-plugin:2.21.0:test (default-test) @ boi-cicd-demo ---
[INFO]
[INFO] --- mule-app-maven-plugin:1.2:mule (default-mule) @ boi-cicd-demo ---
[INFO] Copying classes directly
[INFO] Copying api directly
[INFO] /Users/juan.porcel/AnypointStudio/workspace644/boi-cicd-demo-api/mappings does not exist, skipping
[INFO] Building zip: /Users/juan.porcel/AnypointStudio/workspace644/boi-cicd-demo-api/target/boi-cicd-demo-1.0.0-SNAPSHOT.zip
[INFO]
[INFO] --- maven-install-plugin:2.5.2:install (default-install) @ boi-cicd-demo ---
[INFO] No primary artifact to install, installing attached artifacts instead.
[INFO] Installing /Users/juan.porcel/AnypointStudio/workspace644/boi-cicd-demo-api/pom.xml to /Users/juan.porcel/.m2/repository/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/boi-cicd-demo-1.0.0-SNAPSHOT.pom
[INFO] Installing /Users/juan.porcel/AnypointStudio/workspace644/boi-cicd-demo-api/target/boi-cicd-demo-1.0.0-SNAPSHOT.zip to /Users/juan.porcel/.m2/repository/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/boi-cicd-demo-1.0.0-SNAPSHOT.zip
[INFO]
[INFO] --- mule-app-maven-plugin:1.2:install (default-install) @ boi-cicd-demo ---
[WARNING] MULE_HOME is not set, not copying boi-cicd-demo-1.0.0-SNAPSHOT.zip
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 1.899 s
[INFO] Finished at: 2018-05-08T20:02:01-03:00
[INFO] ------------------------------------------------------------------------
juan.porcel@BA-JPORCE-OSX:~/AnypointStudio/workspace644/boi-cicd-demo-api$ mvn org.mule.tools.maven:mule-maven-plugin:2.2.1:deploy
[INFO] Scanning for projects...
[INFO]
[INFO] ---------< 65e99896-9aea-448e-b484-9a13e3f23349:boi-cicd-demo >---------
[INFO] Building boi-cicd-demo-api 1.0.0-SNAPSHOT
[INFO] --------------------------------[ mule ]--------------------------------
[INFO]
[INFO] --- mule-maven-plugin:2.2.1:deploy (default-cli) @ boi-cicd-demo ---
[INFO] No application configured. Using project artifact: /Users/juan.porcel/.m2/repository/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/boi-cicd-demo-1.0.0-SNAPSHOT.zip
[INFO] Found application boi-cicd-demo on cluster jmporcel-test-cluster-15. Redeploying application...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 13.923 s
[INFO] Finished at: 2018-05-08T20:02:19-03:00
[INFO] ------------------------------------------------------------------------
juan.porcel@BA-JPORCE-OSX:~/AnypointStudio/workspace644/boi-cicd-demo-api$
```

For a full list and description of the supported parameters by the Mule Maven Plugin, read https://docs.mulesoft.com/mule-user-guide/v/3.8/mule-maven-plugin#runtime-manager.

For other depoloyment models please refer to https://docs.mulesoft.com/mule-user-guide/v/3.8/mule-maven-plugin

4.2. Deploy to ARM and Deliver Exchange.

Execute `mvn clean deploy -Danypoint_user=<username> -Danypoint_pass=<password>`.
This will first package the artifact (Mule Application) and deploy to the target runtime, as well to the target repository (ex. Exchange).


Example console output
```
juan.porcel@BA-JPORCE-OSX:~/AnypointStudio/workspace644/boi-cicd-demo-api$ mvn clean deploy -Danypoint_user=myuser -Danypoint_pass=mypass
[INFO] Scanning for projects...
[INFO]
[INFO] ---------< 65e99896-9aea-448e-b484-9a13e3f23349:boi-cicd-demo >---------
[INFO] Building boi-cicd-demo-api 1.0.0-SNAPSHOT
[INFO] --------------------------------[ mule ]--------------------------------
[INFO]
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ boi-cicd-demo ---
[INFO] Deleting /Users/juan.porcel/AnypointStudio/workspace644/boi-cicd-demo-api/target
[INFO]
[INFO] --- mule-app-maven-plugin:1.2:attach-test-resources (default-attach-test-resources) @ boi-cicd-demo ---
[INFO] attaching test resource /Users/juan.porcel/AnypointStudio/workspace644/boi-cicd-demo-api/src/main/app
[INFO]
[INFO] --- build-helper-maven-plugin:1.7:add-resource (add-resource) @ boi-cicd-demo ---
[INFO]
[INFO] --- maven-resources-plugin:3.0.2:resources (default-resources) @ boi-cicd-demo ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources
[INFO] Copying 3 resources
[INFO] Copying 1 resource
[INFO] skip non existing resourceDirectory /Users/juan.porcel/AnypointStudio/workspace644/boi-cicd-demo-api/mappings
[INFO]
[INFO] --- mule-app-maven-plugin:1.2:filter-resources (default-filter-resources) @ boi-cicd-demo ---
[INFO]
[INFO] --- maven-compiler-plugin:3.7.0:compile (default-compile) @ boi-cicd-demo ---
[INFO] Nothing to compile - all classes are up to date
[INFO]
[INFO] --- maven-resources-plugin:3.0.2:testResources (default-testResources) @ boi-cicd-demo ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 1 resource
[INFO] Copying 3 resources
[INFO]
[INFO] --- maven-compiler-plugin:3.7.0:testCompile (default-testCompile) @ boi-cicd-demo ---
[INFO] Nothing to compile - all classes are up to date
[INFO]
[INFO] --- maven-surefire-plugin:2.21.0:test (default-test) @ boi-cicd-demo ---
[INFO]
[INFO] --- mule-app-maven-plugin:1.2:mule (default-mule) @ boi-cicd-demo ---
[INFO] Copying classes directly
[INFO] Copying api directly
[INFO] /Users/juan.porcel/AnypointStudio/workspace644/boi-cicd-demo-api/mappings does not exist, skipping
[INFO] Building zip: /Users/juan.porcel/AnypointStudio/workspace644/boi-cicd-demo-api/target/boi-cicd-demo-1.0.0-SNAPSHOT.zip
[INFO]
[INFO] --- maven-install-plugin:2.5.2:install (default-install) @ boi-cicd-demo ---
[INFO] No primary artifact to install, installing attached artifacts instead.
[INFO] Installing /Users/juan.porcel/AnypointStudio/workspace644/boi-cicd-demo-api/pom.xml to /Users/juan.porcel/.m2/repository/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/boi-cicd-demo-1.0.0-SNAPSHOT.pom
[INFO] Installing /Users/juan.porcel/AnypointStudio/workspace644/boi-cicd-demo-api/target/boi-cicd-demo-1.0.0-SNAPSHOT.zip to /Users/juan.porcel/.m2/repository/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/boi-cicd-demo-1.0.0-SNAPSHOT.zip
[INFO]
[INFO] --- mule-app-maven-plugin:1.2:install (default-install) @ boi-cicd-demo ---
[WARNING] MULE_HOME is not set, not copying boi-cicd-demo-1.0.0-SNAPSHOT.zip
[INFO]
[INFO] --- maven-deploy-plugin:2.8.2:deploy (default-deploy) @ boi-cicd-demo ---
[INFO] No primary artifact to deploy, deploying attached artifacts instead.
Downloading from exchange2-boi-eu-repository: https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/65e99896-9aea-448e-b484-9a13e3f23349/maven/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/maven-metadata.xml
Downloaded from exchange2-boi-eu-repository: https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/65e99896-9aea-448e-b484-9a13e3f23349/maven/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/maven-metadata.xml (284 B at 151 B/s)
Uploading to exchange2-boi-eu-repository: https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/65e99896-9aea-448e-b484-9a13e3f23349/maven/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/boi-cicd-demo-1.0.0-20180508.231258-1.pom
Uploaded to exchange2-boi-eu-repository: https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/65e99896-9aea-448e-b484-9a13e3f23349/maven/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/boi-cicd-demo-1.0.0-20180508.231258-1.pom (6.7 kB at 2.8 kB/s)
Downloading from exchange2-boi-eu-repository: https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/65e99896-9aea-448e-b484-9a13e3f23349/maven/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/maven-metadata.xml
Downloaded from exchange2-boi-eu-repository: https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/65e99896-9aea-448e-b484-9a13e3f23349/maven/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/maven-metadata.xml (269 B at 383 B/s)
Uploading to exchange2-boi-eu-repository: https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/65e99896-9aea-448e-b484-9a13e3f23349/maven/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/maven-metadata.xml
Uploaded to exchange2-boi-eu-repository: https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/65e99896-9aea-448e-b484-9a13e3f23349/maven/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/maven-metadata.xml (626 B at 426 B/s)
Uploading to exchange2-boi-eu-repository: https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/65e99896-9aea-448e-b484-9a13e3f23349/maven/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/maven-metadata.xml
Uploaded to exchange2-boi-eu-repository: https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/65e99896-9aea-448e-b484-9a13e3f23349/maven/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/maven-metadata.xml (348 B at 240 B/s)
Uploading to exchange2-boi-eu-repository: https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/65e99896-9aea-448e-b484-9a13e3f23349/maven/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/boi-cicd-demo-1.0.0-20180508.231258-1.zip
Uploaded to exchange2-boi-eu-repository: https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/65e99896-9aea-448e-b484-9a13e3f23349/maven/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/boi-cicd-demo-1.0.0-20180508.231258-1.zip (4.7 kB at 2.1 kB/s)
Uploading to exchange2-boi-eu-repository: https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/65e99896-9aea-448e-b484-9a13e3f23349/maven/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/maven-metadata.xml
Uploaded to exchange2-boi-eu-repository: https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/65e99896-9aea-448e-b484-9a13e3f23349/maven/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/maven-metadata.xml (799 B at 548 B/s)
[INFO]
[INFO] --- mule-maven-plugin:2.2.1:deploy (mule-deploy) @ boi-cicd-demo ---
[INFO] No application configured. Using project artifact: /Users/juan.porcel/.m2/repository/65e99896-9aea-448e-b484-9a13e3f23349/boi-cicd-demo/1.0.0-SNAPSHOT/boi-cicd-demo-1.0.0-SNAPSHOT.zip
[INFO] Found application boi-cicd-demo on cluster jmporcel-test-cluster-15. Redeploying application...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 30.609 s
[INFO] Finished at: 2018-05-08T20:13:25-03:00
[INFO] ------------------------------------------------------------------------
```

* Note. Once the asset is published to exchange, that asset GAV (Group Id, Asset Id, Version) cannot be overwritten (redeployed). While trying to do so a 409 error will be received:
```
Return code is: 409, ReasonPhrase: An asset with the given GroupId, AssetId and Version already exists..
```
The good practice is to increment asset version for each deployment.



### Publishing API to Exchange

Include Exchange Maven Facade Configuration to your pom.xml

```xml
	<repositories>
		<repository>
			<id>Central</id>
			<name>Central</name>
			<url>http://repo1.maven.org/maven2/</url>
			<layout>default</layout>
		</repository>
		<repository>
			<id>mulesoft-releases</id>
			<name>MuleSoft Releases Repository</name>
			<url>http://repository.eu1.mulesoft.org/releases/</url>
			<layout>default</layout>
		</repository>
		<repository>
			<id>Exchange2_EU</id>
			<name>Exchange2 Repository EU</name>
			<url>https://maven.eu1.anypoint.mulesoft.com/api/v1/organizations/${anypoint.org.id}/maven</url>
		</repository>
	</repositories>
```

## API Promotion
TBD

## API Policies

This guideline provides details on automating the management of API Policy Templates (Custom Policies) creation, and applying  API policies available in API Manager to existing API instances.
The assets for API Policies Automation are in `api-policies-automation` folder.

### API Manager CLI

The API Manager CLI consists on a client application (Bash Script) that can be leverage to automate the following operations:
* Policy Templates creation
* Policy Templates deletion
* Application of a custom policy to an existing API.(1)
* Application of an out-of-the-box policy to an existing APIs.(1)
* Delete a custom policy template.

(1) The policy will be applied to all the available instances of the target API (same API Name and Version) within the specified environment.

#### Usage
These section describe the use cases for which this CLI can be used within a CICD pipeline for Policy management automation.
Example of the different operations using the API Manager CLI
```
./api_manager_cli.sh ( list | create-policy | delete-policy | apply-policy  ) [ --region eu1 ] -u|--username {username} -p|--password {password} -o|--organization {orgName} -e|--environment {envName}
```

#### Example - list existing policy templates
```
./api_manager_cli.sh list-policies --region eu1 -u username -p password -o "My Anypoint Org"
```

#### Example - create a policy template
```
 ./api_manager_cli.sh create-policy --region eu1 -u username -p password -o "My Anypoint Org" --policy-name "cicd-sample-policy"     --policy-definition "cicd-sample-policy/dn-cid-policy.yaml"     --policy-configuration "cicd-sample-policy/dn-cid-policy.xml"
```

#### Example - delete a policy template
```
 ./api_manager_cli.sh delete-policy --region eu1 -u username -p password -o "My Anypoint Org"  --policy-name "cicd-sample-policy"
```
#### Example - apply a custom policy
```
 $> ./api_manager_cli.sh  apply-policy --policy-type custom --region eu1 -u username -p password -o "My Anypoint Org" -e "Sandbox" --policy-name  --api-name "API Name"  --api-version "1.0.0" --policy-data '{ "attribute" : "value" }'
```

#### Limitations
* A policy template can only be deleted if it is not applied to any API instance. Otherwise, an error response will be received.
Example:
```json
{"name":"InvalidOperationError","message":"Custom policy template cannot be deleted as it is applied to the following APIs:\n  - groupId:65e99896-9aea-448e-b484-9a13e3f23349:assetId:notes-example-api - 2.0.2:2633\n  - groupId:65e99896-9aea-448e-b484-9a13e3f23349:assetId:notes-example-api - 2.0.2:2893\n  - groupId:65e99896-9aea-448e-b484-9a13e3f23349:assetId:notes-example-api - 2.0.2:2633\n  - groupId:65e99896-9aea-448e-b484-9a13e3f23349:assetId:notes-example-api - 2.0.2:2893"}
```
