// ===================================================================================
// === MuleSoft
// === version: 1.0					                                               ===
// === Description: 					                                           ===
//     Script manages promotion of configured applications from source environment ===
//     to target environment. E.g. from TEST to PROD 	 				   		   ===
// ===================================================================================
console.log('------ Anypoint API Promotion Client has started ------');

//used libs
const Utility = require('./utility');
const Arm = require('./arm_api_wrapper');
const Manager = require('./manager_api_wrapper');
const Common = require('./common_api_wrapper');

var config = Utility.getConfig();

var anypointInfo = {};

var arg = Utility.getArgument(); //validate argument

//access environment variable
console.log('User: "' + process.env.ANYPOINT_USER + '" is connecting to anypoint');

//main logic
if(arg == Utility.APP_ONLY_PARAM) {
	runApplicationPromotion();
} else {
	runApiPromotion();
}
//end of main logic

/*
 * Triggers API promotion logic. Implements the whole integration flow.
 */
function runApiPromotion() {
	Common.getAnypointInfo(config)
	.then((anyInfo) => {
		anypointInfo = anyInfo;
		console.log('------ API(s) Promotion (APIM) has started ------');
		return Manager.promoteApis(config.applications, anypointInfo);
	})
	.then((apiInstances) => {
		console.log("Promoted API Instances: " + JSON.stringify(apiInstances));
		console.log("------ API(s) Promotion (APIM) finalized ------");

		//trigger APP deployment / promotion immediatelly
		console.log("------ Application(s) Promotion (ARM) has been initialized ------");
		runApplicationPromotion(apiInstances);

	})
	.catch(err => {
		console.log("Error: " + err);
		process.exit(-1);
	});

}

/*
 * Triggers application promotion logic. Implements the whole integration flow.
 * Function accepts api instances as input to be able to register promoted applications with api instances in API Manager.
 * Application can be patched with or without updating API Instance.
 * It is highly recommended NOT to trigger fresh app deployment without registering with API Manager - without promoting API
 * together with application.
 */
function runApplicationPromotion(apiInstances) {
	Common.getAnypointInfo(config)
	.then((anyInfo) => {
		anypointInfo = anyInfo;
		return Arm.getApplications(anypointInfo.token, anypointInfo.orgId, anypointInfo.sourceEnvId, config.applications);
	})
	.then((applications) => {
		console.log("Applications to be promoted: " + JSON.stringify(applications));

		//deploy all the applications
		return Promise.all(applications.map((application) => {
			console.log("Application: " + JSON.stringify(application));

    		return Arm.redeployApplication(anypointInfo.token, anypointInfo.orgId,
    			anypointInfo.targetEnvId, anypointInfo.runtimeTargetId,
    			application.appName, application.appId, application.apiInstanceId, apiInstances);
		}));
	})
	.then((result) => {
		console.log("------ Deployment Finalized! Find responses below: ------ \n" + JSON.stringify(result));
	})
	.catch(err => {
		console.log("Error: " + err);
		process.exit(-1);
	});
}
