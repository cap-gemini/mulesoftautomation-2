const Arm = require('./arm_api_wrapper');
const yaml = require('js-yaml');
const fs = require('fs');

const CONFIG_FILE = "config/promotion_config.yml";
const TYPE_SERVER_CONST = "SERVER";
const TYPE_CLUSTER_CONST = "CLUSTER";
const SOURCE_ENV_ID = "SOURCE_ENV_ID";
const TARGET_ENV_ID = "TARGET_ENV_ID";
const APP_ONLY_PARAM = "app-only";


const config = loadConfiguration();

/*
 * Return app configuration object
 */
function getConfig() {
  return config;
}

function loadConfiguration() {
	try {
			console.log("Loading app Configuration.");
    	const configFile = yaml.safeLoad(fs.readFileSync(CONFIG_FILE, 'utf8'));
    	const indentedJson = JSON.stringify(configFile, null, 4);

			var configuration = JSON.parse(indentedJson);

			//US default region
			var anypointPlatformBaseUrl = "https://anypoint.mulesoft.com";
			if ( configuration.region == "EU1" ) {
				anypointPlatformBaseUrl="https://eu1.anypoint.mulesoft.com";
			};
			configuration.anypointPlatformBaseUrl = anypointPlatformBaseUrl;

			console.log(configuration);

    	return configuration;
	} catch (e) {
    	console.log("Problem to load configuration file: " + e);
    	process.exit(-1);
	}
}


/*
 * Validate input argument and return its value
 */
function getArgument() {
    if (process.argv.length <= 2) {
        return null;
    }

    var param = process.argv[2];

    if(param != APP_ONLY_PARAM) {
        console.log("Invalid argument: " +
            "please pass one of the valid arugments: \'%s\'", APP_ONLY_PARAM);
        process.exit(-1);
    }

    return param;
}

/*
 * Build array of promises used to obtain source runtime ID and target runtime ID
 */
function buildRuntimesPromises(token, orgId,
	targetType, targetName, targetId, sourceType, sourceName, sourceId, runtimeVersion) {

    var promiseArray = [];
    if(targetType == TYPE_SERVER_CONST && sourceType == TYPE_SERVER_CONST) {
    	promiseArray = [Arm.getServer(token, orgId, targetId, targetName, runtimeVersion), Arm.getServer(token, orgId, sourceId, sourceName, runtimeVersion)];
    } else if(targetType == TYPE_CLUSTER_CONST && sourceType == TYPE_CLUSTER_CONST) {
    	promiseArray = [Arm.getCluster(token, orgId, targetId, targetName, runtimeVersion), Arm.getCluster(token, orgId, sourceId, sourceName, runtimeVersion)];
    } else if (targetType == TYPE_SERVER_CONST && sourceType == TYPE_CLUSTER_CONST) {
    	promiseArray = [Arm.getServer(token, orgId, targetId, targetName, runtimeVersion), Arm.getCluster(token, orgId, sourceId, sourceName, runtimeVersion)];
    } else if(targetType == TYPE_CLUSTER_CONST && sourceType == TYPE_SERVER_CONST) {
    	promiseArray = [Arm.getCluster(token, orgId, targetId, targetName, runtimeVersion), Arm.getServer(token, orgId, sourceId, sourceName, runtimeVersion)];
	} else {
	    console.log("Error: Unsupported target or source type provided!");
    	process.exit(-1);
    }

    return promiseArray;
}


/*
 * Functionality exported by this module
 */
module.exports.APP_ONLY_PARAM         = APP_ONLY_PARAM;
module.exports.SOURCE_ENV_ID          = SOURCE_ENV_ID;
module.exports.TARGET_ENV_ID          = TARGET_ENV_ID;
module.exports.getConfig 							= getConfig;
module.exports.getArgument            = getArgument;
module.exports.loadConfiguration 			= loadConfiguration;
module.exports.buildRuntimesPromises  = buildRuntimesPromises;
