
# API Promotion Automation

Use this framework to easily promote your APIs and applications across environments.
The script execution is fully configurable, therefore it can support any environment and promote APIs and applications towards any direction.
These scripts are built for Anypoint Management Plane, in the EU1 Region.

Supported scenarios are:

![Scenarios](./images/scenarios.png)

* Promote API, and deploy the application to the target environment (fresh deployment).
* Promote API instance (the original API will be switched to Inactive state), and patch existing application on target server.
* Patch existing application on the target servers - no updates in API Instance / new application version will register with the same API Instance as the old version.

Project also contains pre-build Jenkins pipeline for "one click" deployment to provide some idea on how this tool could be used in terms of CICD.

## Pre-requisites

* Installed Node.js
* Set environment variables:
	* `ANYPOINT_USER` - user with permission to promote APIs (deployment user)
	* `ANYPOINT_PASSWORD` - user password
* API created in source environment, corresponding application instance deployed in source runtime via Runtime Manager.

## Supported versions

* Anypoint Platform Crowd 2
* API: [ARM - Anypoint Runtime Manager 1.23](https://anypoint.mulesoft.com/apiplatform/anypoint-platform/#/portals/organizations/ae639f94-da46-42bc-9d51-180ec25cf994/apis/38784/versions/1490649/pages/182845)
* API: [API Manager v1](https://anypoint.mulesoft.com/exchange/portals/anypoint-platform-eng/f1e97bc6-315a-4490-82a7-23abe036327a.anypoint-platform/api-manager-api/api/v1/pages/Promoting%20an%20API/)
* Tested on Mule Runtimes: 3.8.5, 3.9.0
* Tested with Node.js v8.4.0 and npm 5.8.0

## Considerations

* API Auto-discovery automated update for promoted APIs:
  * Supported for Mule Version 3.9+
  * Not supported for Mule Version 3.8.x
  
* Anypoint Platform multi-region support (US, EU1).

* Runtime Deployment Model Supported: Hybrid.

* Runtime Manager: The tool supports application PROMOTION only, i.e. the application must be running in the Runtime Manager source environment.

* API Manager: The tool supports API PROMOTION only, i.e. the API must exist in the API Manager source environment.

* Deployment of external properties file is not supported. Properties file must be copied to server before this script runs.

* Every API Instance promotion would create a new API Instance on target environment. Patching of API Instance is not supported.


## Configuration


```yaml
region: "EU1"                             # Anypoint Platform (Control Plane) Region ("US", "EU1" values supported).
org: "Bank Of Ireland"                    # Organisation / Business Group name
sourceEnvName: "DEV"                      # Name of environment configure on ARM
sourceServerName: "demo-cluster-dev"      # Source runtime name - could be server or cluster
sourceServerType: "CLUSTER"               # Supported types are SERVER or CLUSTER
targetEnvName: "Sandbox"                  # Name of environment configured on ARM
targetServerName: "demo-cluster-sandbox"  # Target runtime name - could be server or cluster
targetServerType: "CLUSTER"               # Supported types are SERVER or CLUSTER
runtimeVersion: "3.8.5"                   # Mule Runtime Version used
applications:                             # All the applications running on source runtime that should be promoted to target runtime
    -
      appName: "notes-example-api"        # Application name as displayed on the source server
      apiInstanceId: 4888                 # API Instance ID (Available in API Manager / App Configuration)

```

## How to run
1. Update configuration file as per your requirements
```
config/promotion_config.yml
```
2. Use npm to install project dependencies
```
npm install
```
3. Run script app.js to:
	1. Promote API Instances and applications
	```
	node src/app.js
	```
	2. Promote Applications only - patch applications without any updates in registration with API Manager
	```
	node src/app.js app-only
	```

## API Auto-Discovery

This section describes how to enable automated auto-discovery for APIs and applications that have been promoted.

Auto-discovery requires API Version to be configured within the application `<api-platform-gw:api apiName="${api.name}" version="${api.build.version}:${api.instance}" flowRef="api-main" create="true" apikitRef="api-config" doc:name="API Autodiscovery" />`. See documentation for more details [here](https://docs.mulesoft.com/api-manager/v/2.x/configure-auto-discovery-new-task). The API Version is generated as part of the API promotion process (once API Instance is created on specific environment), however application must be aware of the API Version in API Manager to register with API Manager.

For APIs using Runtime Version 3.9 or higher, the solution takes care of setting the promoted API instance Id as part of a property in the application configuration via ARM.
This eliminates the need of updating the application, building and deploying.

For APIs running on Mule 3.8.x, `api.instance` property must be updated in the application properties file. Redeployment of the application will be required.


### Capturing API Version (Mule 3.9+)
How to capture API Version and make it available for application.

After running the command `node src/app.js` API Instances are promoted and API Instance IDs captured, so each application can be registered with proper API Instance as part of the next step: promotion of applications.
The script creates a pairs of origin API Instance ID and newly generated API Instance ID (ID of promoted API Instance on the target environment), then it can easily identify what application needs to use what API Instance ID.

Captured API Instance ID is then used and added to properties in application's settings (**Runtime 3.9+ only**), as displayed on picture below:
![Settings](./images/app-settings.png)

### API Auto-Discovery - Studio Project configuration
How to configure the project / application to use a new API Instance ID generated by promoting the instance from lower environment.

**1. Step**: Configure API Auto-Discovery

API Auto-Discovery online documentation: https://docs.mulesoft.com/api-manager/v/2.x/configure-auto-discovery-new-task

**Mule 3.8.x / Mule 3.9.x** 
Reference documentation: https://docs.mulesoft.com/api-manager/v/2.x/configure-autodiscovery-3-task

In your application Mule config file (xml) add:

```xml
<api-platform-gw:api apiName="${api.name}" version="${api.raml.version}:${api.instance}" flowRef="api-main" create="true" apikitRef="api-config" doc:name="API Autodiscovery" />
```
`api.raml.version` and `api.instance` must be defined within a properties file in the application.

Example:
```
#API Autodiscovery
api.name=groupId:65e99896-9aea-448e-b484-9a13e3f23349:assetId:notes-example-api
api.raml.version=2.0.2
api.instance=5097
```

***Note*** 

`api.raml.version` and `api.instance` are set in the configuration of the auto-discovery element: `version="${api.build.version}:${api.instance}"`.
For this specific example we would get: `version="2.0.2:${api.instance}"`.
Property from settings `api.instance` is then used during the application deployment / start up to replace variable `${api.instance}`
Finally we are getting `version="2.0.2:9547246"` and auto-discovery is enabled.

**Important**
After API promotion, `api.instance` value will be set by the script with the new promoted API instance id value, via ARM Properties. This is only supported for 3.9+ version.
APis running on Mule 3.8.x will require to update the`api.instance` property in the properties file of the promoted application, and redeploy.

**Mule 4**

Reference documentation: https://docs.mulesoft.com/api-manager/v/2.x/configure-autodiscovery-4-task

```xml
<api-gateway:autodiscovery apiId="${api.instance}" flowRef="proxy" />
```

In Mule 4, the API Instance ID is used instead of the full API version.


**2. Step**: Configure Maven to enable filtering of application directory. 

Add the following to your `pom.xml` for plugin `mule-app-maven-plugin`: `<filterAppDirectory>true</filterAppDirectory>`.

```xml
<plugin>
  <groupId>org.mule.tools.maven</groupId>
  <artifactId>mule-app-maven-plugin</artifactId>
  <version>${mule.tools.version}</version>
  <extensions>true</extensions>
  <configuration>
    <copyToAppsDirectory>true</copyToAppsDirectory>
    <filterAppDirectory>true</filterAppDirectory>
  </configuration>
</plugin>
```


## Continuous Deployment
Project also contains `Jenkinsfile` with simple pipeline definition for easy integration with Jenkins. Pipeline implements "one click" deployment and is configured to be triggered manually.
The same environment variables as mentioned in [**Pre-requisites**](#pre-requisites) section must be configured on Jenkins server.

#### Pipeline consists of the following stages:

**1. Stage**:  Promote APIs and Applications (exectues when build parameter APP_ONLY == false): runs command `node src/app.js`

**2. Stage**:  Promote Applications only (executes when build parameter APP_ONLY == true): runs command `node src/app.js app-only`

## Notes
* Custom policies are supported. The tool will promote API Instance with all the configured policies, custom policies included.

* New API Instance creation means that your client applications must request access for this new version (e.g. if Client ID enforcement policy has been applied).

* Difference between **API Instance ID** and **API Version** can be seen in API Manager as highlighted on the picture below:
![API Instance ID vs API Version](./images/api-manager.png)
