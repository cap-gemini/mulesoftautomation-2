#!/bin/bash

if [ -z "$1" ]; then
   echo "ERROR - Docker Image TAG input parameter is missing."
   exit 1
fi
image_tag=$1
if [ -z "$2" ]; then
    echo "Using Docker Image TAG as container name suffix."
    container_suffix=$1
else
  container_suffix=$2
fi
mule_version=3.8.5
repository=mule-ee-runtime-${mule_version}
container=mule-ee-runtime-${mule_version}-${container_suffix}
# From this shared directory between the host operating system and the Docker container, you will have access to log files.
mule_logs_dir="$(pwd)/container-logs/${container}"

echo "Creating shared logs directory on host [ ${mule_logs_dir} ]"
mkdir -v -p ${mule_logs_dir}
echo "Running Docker Container: ${container}"
docker run -it -d -p 8081:8081 -v "${mule_logs_dir}":/opt/mule/logs --name ${container} ${repository}:${image_tag}
docker ps -a
echo "TIP #1: CHECK CONTAINER LIST AND STATUS: docker ps -a"
echo "TIP #2: REGISTER THE CONTAINER NODE: docker exec -it  ${container} /opt/mule/bin/arm_register_server.sh -o \"ORG_NAME\" -e \"ENVIRONMENT_NAME\" -s \"SERVER_NAME\" -c \"CLUSTER_NAME\"  -u \"USER\" -p \"PASSWORD\"  --region eu1 "
echo "TIP #3 RESTART MULE RUNTIME: docker exec -it ${container} /opt/mule/bin/mule restart"
